const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
  name: { type: String, required: true },
  imageName: { type: String, required: true },
  price: { type: Number },
  stock: { type: Number, default: 10 },
});

const ItemModel = mongoose.model("Item", itemSchema);

const items = [
    new ItemModel({name: 'Coke', imageName: 'coke', price: 10}), 
    new ItemModel({name: 'Pepsi', imageName: 'pepsi', price: 10}),
    new ItemModel({name: 'Dew', imageName: 'dew', price: 10})
];

mongoose
  .connect('mongodb+srv://admin:admin@cluster0.8a9qe.mongodb.net/vendingmachine?retryWrites=true&w=majority')
  .catch(err => {
    console.log(err.stack);
    process.exit(1);
  })
  .then(() => {
    console.log("connected to db");
    ItemModel.deleteMany({}, () => {
        items.map(async (item, index) => {
            await item.save((err, result) => {
              if(err) console.log("Error seeding", err);
              else console.log("Seeded Item", item.name)
              if (index === items.length - 1) {
                console.log("DONE!");
                mongoose.disconnect();
              }
            });
          });
    });
  });