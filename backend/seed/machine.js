const mongoose = require("mongoose");

const machineSchema = new mongoose.Schema({
  coin: { type: Number, default: 100 },
  cash: { type: Number, default: 200 },
});

const MachineModel = mongoose.model("Machine", machineSchema);

const machine = new MachineModel();

mongoose
  .connect('mongodb+srv://admin:admin@cluster0.8a9qe.mongodb.net/vendingmachine?retryWrites=true&w=majority')
  .catch(err => {
    console.log(err.stack);
    process.exit(1);
  })
  .then(() => {
    MachineModel.deleteMany({}, async () => {
      await machine.save((err, result) => {
        if(err) console.log("Error seeding", err);
        else console.log("Seeded Machine")
        mongoose.disconnect();
      });
    }); 
  });