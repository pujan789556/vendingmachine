import { startSession } from 'mongoose';
import { CustomerModel } from '../../models/customer.model'
import { ItemModel } from '../../models/item.model'
import { OrderModel } from '../../models/order.model'
import { MachineModel } from '../../models/machine.model';

const orderItem = (data: any) => new Promise(async (resolve, reject) => {
    const session = await startSession();
    try {
        session.startTransaction();
        const item:any = await ItemModel.findById(data.item);
        const customer:any = await CustomerModel.findById(data.customer);
        if(item && item.stock < 1) {
            reject(new Error(`${item.name} is out of stock`))
        } else if(customer.cash + customer.coin < item.price) {
            reject(new Error('Not sufficient balance'));
        } else {
            const order:any = new OrderModel(data);

            let cash = 0;
            let coin = 0;
            if(customer.cash >= item.price) {
                cash = item.price;
            } else {
                coin = item.price;
            }
            const machine:any = await MachineModel.find({});
            await order.save();
            await ItemModel.findByIdAndUpdate(data.item, {stock: item.stock - 1});
            await CustomerModel.findByIdAndUpdate(data.customer, {cash: customer.cash - cash, coin: customer.coin - coin, return: customer.return === 0 ? (customer.cash + customer.coin) - (cash + coin) : customer.return - (cash + coin)});
            await MachineModel.findByIdAndUpdate(machine[0]._id, {cash: machine[0].cash + cash, coin: machine[0].coin + coin})
            await session.commitTransaction()
            const newOrder = await OrderModel.findById(order._id).populate('item').populate('customer');
            const machineDetail = await MachineModel.findById(machine[0]._id)
            const items = await ItemModel.find({});
            resolve({order: newOrder, machine: machineDetail, items});
        }
    } catch (error) {
        await session.abortTransaction()
        reject(error);
    } finally {
        session.endSession();
    }
});

const refundItem = (id: string) => new Promise(async (resolve, reject) => {
    const session = await startSession();
    try {
        session.startTransaction();
        const order:any = await OrderModel.findByIdAndUpdate(id,{refunded: true});
        const item:any = await ItemModel.findById(order.item);
        const customer:any = await CustomerModel.findById(order.customer);
        const machine:any = await MachineModel.find({});

        let cash = 0;
        let coin = 0;
        if(machine[0].cash >= item.price) {
            cash = item.price;
        } else {
            coin = item.price;
        }

        await ItemModel.findByIdAndUpdate(item._id, {stock: item.stock + 1});
        await CustomerModel.findByIdAndUpdate(customer.id, {cash: customer.cash + cash, coin: customer.coin + coin, return: customer.return === 0 ? 0 : customer.return + (cash + coin)});
        await MachineModel.findByIdAndUpdate(machine[0]._id, {cash: machine[0].cash - cash, coin: machine[0].coin - coin})
        await session.commitTransaction()
        const orders = await OrderModel.find({customer: customer._id, refunded: false}).populate('item').populate('customer');
        const machineDetail = await MachineModel.findById(machine[0]._id);
        const items = await ItemModel.find({});
        resolve({orders, machine: machineDetail, items});
    } catch (error) {
        await session.abortTransaction()
        reject(error);
    } finally {
        session.endSession();
    }
});

export {
    orderItem,
    refundItem
}