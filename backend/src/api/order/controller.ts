import { orderItem, refundItem } from './repository';

const orderController = async (req: any, res: any, _next: any) => {
    try {
        const data =  req.body;
        const order = await orderItem(data);
        res.status(200).json(order);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to order item'});
    }
}

const refundController = async (req: any, res: any, _next: any) => {
    try {
        const { orderId }  = req.query;
        const orders = await refundItem(orderId);
        res.status(200).json(orders);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to refund item'});
    }
}

export {
    orderController,
    refundController
}