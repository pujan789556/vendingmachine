import { Router } from 'express';
import { orderController, refundController} from './controller';

const router = Router();

router.post('/', orderController);
router.post('/refund', refundController);

export default router;