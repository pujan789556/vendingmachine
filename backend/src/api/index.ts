import { Router } from 'express';
import customerRoute from './customer';
import orderRoute from './order';
import itemsRoute from './items';
import machineRoute from './machine';

const router = Router();

router.use('/customer', customerRoute);
router.use('/order', orderRoute);
router.use('/items', itemsRoute);
router.use('/machine', machineRoute);

export default router;