import { Router } from 'express';
import { getMachineController } from './controller';

const router = Router();

router.get('/', getMachineController);
export default router;