import { getMachine } from './repository';

const getMachineController = async (_req: any, res: any, _next: any) => {
    try {
        const machine =  await getMachine();
        res.status(200).json(machine);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to fetch machine'});
    }
}


export {
    getMachineController
}