import { MachineModel } from '../../models/machine.model'

const getMachine = () => new Promise(async (resolve, reject) => {
    try {
        const machines = await MachineModel.find({});
        resolve(machines[0]);
    } catch (error) {
        reject(error);
    }
});

export {
    getMachine
}