import { Router } from 'express';
import { addCustomerController, updateCustomerController, getCurrentCustomerController} from './controller';

const router = Router();

router.post('/', addCustomerController);
router.put('/', updateCustomerController);
router.get('/current',getCurrentCustomerController);
export default router;