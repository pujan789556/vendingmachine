import { createCustomer, updateCustomer, getCustomer } from './repository';

const addCustomerController = async (_req: any, res: any, _next: any) => {
    try {
        const customer =  await createCustomer();
        res.status(200).json(customer);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to create customer'});
    }
}

const updateCustomerController = async (req: any, res: any, _next: any) => {
    try {
        const { id }  = req.query;
        const data = req.body;
        const customer = await updateCustomer(id, data);
        res.status(200).json(customer);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to update customer data'});
    }
}

const getCurrentCustomerController = async (req: any, res: any, _next: any) => {
    try {
        const {id} = req.query;
        const cusomer =  await getCustomer(id);
        res.status(200).json(cusomer);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to fetch customer'});
    }
}

export {
    addCustomerController,
    updateCustomerController,
    getCurrentCustomerController
}