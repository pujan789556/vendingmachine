import { CustomerModel } from '../../models/customer.model'

const createCustomer = () => new Promise((resolve, reject) => {
    try {
        const customer = new CustomerModel();
        customer.save((err) => {
            if (err) reject(err);
            resolve(customer);
          });
    } catch (error) {
        reject(error);
    }
});

const updateCustomer = (id: string, data: any) => new Promise(async (resolve, reject) => {
    try {
        const customer:any = await CustomerModel.findById(id);
        const updateData = {cash: customer.cash + (data.cash || 0), coin: customer.coin + (data.coin || 0)}
        await CustomerModel.findByIdAndUpdate(id, updateData);
        const updatedCustomer = await CustomerModel.findById(id);
        resolve(updatedCustomer);
    } catch (error) {
        reject(error);
    }
});

const getCustomer = (id:string) => new Promise(async (resolve, reject) => {
    try {
        const customer = await CustomerModel.findById(id);
        resolve(customer);
    } catch (error) {
        reject(error);
    }
});


export {
    createCustomer,
    updateCustomer,
    getCustomer
}