import { ItemModel } from '../../models/item.model'

const getItems = () => new Promise(async (resolve, reject) => {
    try {
        const items = await ItemModel.find({});
        resolve(items);
    } catch (error) {
        reject(error);
    }
});

export {
    getItems
}