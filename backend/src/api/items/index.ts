import { Router } from 'express';
import { getItemsController } from './controller';

const router = Router();

router.get('/', getItemsController);
export default router;