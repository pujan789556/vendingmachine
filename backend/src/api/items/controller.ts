import { getItems } from './repository';

const getItemsController = async (_req: any, res: any, _next: any) => {
    try {
        const items =  await getItems();
        res.status(200).json(items);
    } catch (error) {
        res.status(error.code ? error.code : 400)
        .json({message: error.message ? error.message : 'Failed to fetch items'});
    }
}


export {
    getItemsController
}