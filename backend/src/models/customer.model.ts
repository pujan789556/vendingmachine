import { Document, Schema, model } from 'mongoose';

export interface ICustomer extends Document {
    coin: number;
    cash: number;
    return: number;
}

const CustomerSchema = new Schema<ICustomer>({
    coin: {
        type: Number,
        default: 0
    },
    cash: {
        type: Number,
        default: 0
    },
    return: {
        type: Number,
        default: 0
    }
});

export const CustomerModel = model<ICustomer>("Customer", CustomerSchema);