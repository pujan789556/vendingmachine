import { Document, Schema, model, Types } from 'mongoose';
import { ICustomer } from './customer.model';
import { IItem } from './item.model';

export interface IOrder extends Document {
    item: Types.ObjectId;
    customer:  Types.ObjectId;
    number: number;
    refunded: boolean;
}

const OrderSchema = new Schema<IOrder>({
    item: {
        type: Schema.Types.ObjectId,
        ref: 'Item'
    },
    customer: {
        type: Schema.Types.ObjectId,
        ref: 'Customer'
    },
    refunded: {
        type: Boolean,
        default: false
    }
});

export const OrderModel = model<IOrder>("Order", OrderSchema);