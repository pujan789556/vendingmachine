import { Document, Schema, model } from 'mongoose';

export interface IMachine extends Document {
    coin: number;
    cash: number;
}

const MachineSchema = new Schema<IMachine>({
    coin: {
        type: Number,
        required: true,
        min: [0, 'Out of coin'],
        default: 100
    },
    cash: {
        type: Number,
        required: true,
        min: [0, 'Out of cash'],
        default: 200
    }
});

export const MachineModel = model<IMachine>("Machine", MachineSchema);