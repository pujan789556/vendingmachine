import { Document, Schema, model } from 'mongoose';

export interface IItem extends Document {
    name: string;
    imageName: string;
    price: number;
    stock: number;
}

const ItemSchema = new Schema<IItem>({
    name: {
        type: String,
        required: true,
    },
    imageName: {
        type: String,
    },
    price: {
        type: Number,
        required: true
    },
    stock: {
        type: Number,
        required: true,
        default: 10,
        min: [0, 'Out of stock']
    },
});

export const ItemModel = model<IItem>("Item", ItemSchema);