import dotenv from "dotenv";
import express from "express";
import mongoose from "mongoose";
import api from "./api";
import cors from "cors"
// initialize configuration
dotenv.config();

const app = express();
const port = process.env.SERVER_PORT || 8080;

const uri = process.env.MONGO_CLIENT;
mongoose.connect(uri, (err) => {
    if(err) {
         // tslint:disable-next-line:no-console
        console.log("Error connecting mongodb", err);
        return;
    }
     // tslint:disable-next-line:no-console
    console.log('Connected to database');
})

// Bodyparser Middleware
app.use(express.json());
app.use(express.urlencoded({limit: "50mb", parameterLimit: 500000000 , extended: true }));
app.use(cors({
    credentials: true,
    origin: [
        'http://localhost:3000',
    ],
}));
app.use('/api', api);

// start the Express server
app.listen( port, () => {
    // tslint:disable-next-line:no-console
    console.log( `server started at http://localhost:${ port }` );
} );