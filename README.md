# README #

Vending Machine

### What is this repository for? ###

* This is the repo for interview test for outside.


### How do I get set up? ###

* Install node js
* Clone the repo
* cd backend && npm install
* cd frontend && npm install

## Seed Database

* cd backend/seed
* node item.js && node machine.js

## run application

* cd backend && npm start
* cd frontend && npm start

# Backend: localhost:8080
# Frontend: localhost:3000 