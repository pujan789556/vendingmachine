const axios =  require('axios');
const { notification } =  require('antd');

const showErrorMessage = (message) => {
    notification['error']({
        message: 'Error',
        description:
          message,
      });
}
export const get = (url) => new Promise((resolve, reject) => {
    {
        axios.get(url)
      .then(function (response) {
        resolve(response);
      })
      .catch(function (error) {
        showErrorMessage(error.response.data.message);
        reject()
      })
    }
    
})
export const post = (url, data) => new Promise((resolve, reject) => {
    axios.post(url, data)
    .then(function (response) {
      resolve(response);
    })
    .catch(function (error) {
        console.log(error.response);
      showErrorMessage(error.response.data.message);
      reject();
    })
});

export const put = (url, data) => new Promise((resolve, reject) => {
    axios.put(url, data)
    .then(function (response) {
      resolve(response);
    })
    .catch(function (error) {
      showErrorMessage();
      reject()
    })
})