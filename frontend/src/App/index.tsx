import React from 'react';
import 'antd/dist/antd.css';
import { Row } from 'antd';
import VendingMachine from './container/VendingMachine';

const App: React.FC = () => {
    return (
        <VendingMachine />
    );
};

export default App;
