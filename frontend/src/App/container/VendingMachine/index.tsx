import { Button, Card, Col, Input, InputNumber, Row, Space, Spin } from 'antd';
import { Typography } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import {post, get, put} from '../../../Services/axios';

const { Meta } = Card;
const { Text, Link } = Typography;
const CASH = 'cash';
const COIN = 'coin';

interface CustomerData {
    _id: string;
    cash: number;
    coin: number;
    return: number;
}
interface ItemData {
    _id: string;
    imageName: string;
    name: string;
    price: number;
    stock: number;
}
interface MachineData {
    cash: number;
    coin: number;
}
interface OrderData {
    item: any;
    _id: string;
}

const VendingMachine = () => {
    const [customer, setCustomer] = useState<CustomerData>();
    const [items, setItems] = useState<ItemData[]>([]);
    const [machine, setMachines] = useState<MachineData>();
    const [orders, setOrders] = useState<OrderData[]>([]);
    const [cash, setCash] = useState(0);
    const [coin, setCoin] = useState(0);

    useEffect(() => {
      if(!customer)
       post('http://localhost:8080/api/customer',{}).then((response) => {
           setCustomer(response.data);
       });
       if(items.length === 0)
       get('http://localhost:8080/api/items').then((response) => {
           setItems(response.data);
       });
       if(!machine)
       get('http://localhost:8080/api/machine').then((response) => {
        setMachines(response.data);
    });
    }, []);


    useEffect(() => {
        if(customer){
            get(`http://localhost:8080/api/customer/current?id=${customer?._id}`).then((response) => {
                setCustomer(response.data);
            });
        }
     }, [orders]);

    const orderItem = (id: string) => {
        post('http://localhost:8080/api/order',{item: id, customer: customer?._id}).then((response) => {
            setOrders([...orders, response.data.order]);
            setMachines(response.data.machine);
            setItems(response.data.items);
       });
    }

    const refundItem = (id: string) => {
        post(`http://localhost:8080/api/order/refund?orderId=${id}`).then((response) => {
            setOrders(response.data.orders);
            setMachines(response.data.machine);
            setItems(response.data.items);
       });
    }

    const loadBalance = (value:number, type:string) => {
        put(`http://localhost:8080/api/customer?id=${customer?._id}`,{[type]: value}).then((response) => {
            setCustomer(response.data);
            setCoin(0);
            setCash(0);
       });  
    }
    return (
        <>
            <Row>
           <Col span={18}>
                    <Row>
                        <Card title="Vending Machine Balance">
                            <p>Cash: {machine?.cash}</p>
                            <p>Coin: {machine?.coin}</p>
                        </Card>
                    </Row>
                    <Row>
                            {items?.map((item, index) => {
                                return(
                                    <Col span={6} style={{padding: 10}}>
                                <Card
                                cover={<img src={item?.imageName + '.png'} />}
                                >
                                    <p style={{ fontWeight: 600 }}>{item?.name}</p>
                                    <p>Price: Rs.{item?.price}</p>
                                    <p>On Stock: {item?.stock}</p>
                                    <Button type="primary" onClick={()=>orderItem(item._id)}>
                                        Buy
                                    </Button>
                                </Card>
                                </Col>
                                )
                            })}
                    </Row>
                    <Row>
                <Col span={6}>
                    <Card title="Customer Balance">
                        <p>Cash: {customer?.cash}</p>
                        <p>Coin: {customer?.coin}</p>
                        <p>Return: {customer?.return}</p>
                    </Card>
                </Col>
                <Col span={6}>
                    <Card title="Load Balance">
                    <Space>
                        <InputNumber value={cash} onChange={setCash} />
                        <Button
                            type="primary"
                            onClick={() => {
                                loadBalance(cash, CASH);
                            }}
                            >
                            Load Cash
                        </Button>
                    </Space>
                    <Space style={{marginTop: 10}}>
                        <InputNumber value={coin} onChange={setCoin} />
                        <Button
                            type="primary"
                            onClick={() => {
                                loadBalance(coin, COIN);
                            }}
                            >
                            Load Coin
                        </Button>
                    </Space>
                    </Card>
                </Col>
                </Row>
            </Col>
            <Col span={6}>
                <h3>Orders</h3>
                {orders.map(order => {
                    return (
                    <Card
                    cover={<img src={order?.item?.imageName + '.png'} />}
                    >
                        <p style={{ fontWeight: 600 }}>{order?.item?.name}</p>
                        <p>Price: Rs.{order?.item?.price}</p>
                        <Button type="primary" onClick={()=>refundItem(order?._id)}>
                            Refund
                        </Button>
                    </Card>
                    )
                })}
            </Col>
        </Row>
        </>
    );
};

export default VendingMachine;